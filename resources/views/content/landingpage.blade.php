@extends('Layout.layout')

@section('content')
    {{-- @include('Component.navbar') --}}
    <div class="banner_section layout_padding">
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="">
                <div class="carousel-item active">
                    <div class="container">
                        <h1 class="banner_taital">SING WITH US</h1>
                        <p class="banner_text">Your favorite famous musician will perform just for you</p>
                        <div class="light-button"><a data-toggle="modal" data-target="#exampleModal"
                                onclick="sembunyiRegist()" href="">Order Now</a></div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="container">
                        <h1 class="banner_taital">SING WITH US</h1>
                        <p class="banner_text">Your favorite famous musician will perform just for you</p>
                        <div class="light-button"><a data-toggle="modal" data-target="#exampleModal"
                                onclick="sembunyiRegist()" href="">Order Now</a></div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="container">
                        <h1 class="banner_taital">SING WITH US</h1>
                        <p class="banner_text">Your favorite famous musician will perform just for you</p>
                        <div class="light-button"><a data-toggle="modal" data-target="#exampleModal"
                                onclick="sembunyiRegist()" href="">Order Now</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="services_section trending_padding">
        <div class="container">
            <div class="trending_header header-font">
                <p>Trending</p>
            </div>
            <div class="services_section_2">
                <div class="row">
                    <div class="col-md-4">
                        <div><img src="images/blackpink.png" class="services_img"></div>
                        <div class="light-button upper-font bold-font"><a data-toggle="modal" data-target="#exampleModal"
                                onclick="sembunyiRegist()" href="">BlackPink</a></div>
                    </div>
                    <div class="col-md-4">
                        <div><img src="images/justin.png" class="services_img"></div>
                        <div class="light-button upper-font bold-font"><a data-toggle="modal" data-target="#exampleModal"
                                onclick="sembunyiRegist()" href="">Justin Bieber</a></div>
                    </div>
                    <div class="col-md-4">
                        <div><img src="images/coldplay.png" class="services_img"></div>
                        <div class="light-button upper-font bold-font"><a data-toggle="modal" data-target="#exampleModal"
                                onclick="sembunyiRegist()" href="">Coldplay</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
    </script>
@endsection
