@extends('Layout.loged-layout')

@section('content')
<div class="container">
    <h1>Data Pemesanan</h1>

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <table class="table">
        <thead>
            <tr>
                <th>ID Pemesanan</th>
                <th>ID Pembeli</th>
                <th>ID Tiket</th>
                <th>Jumlah Tiket</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pemesanan as $pesanan)
                <tr>
                    <td>{{ $pesanan->id }}</td>
                    <td>{{ $pesanan->id_pembeli }}</td>
                    <td>{{ $pesanan->id_tiket }}</td>
                    <td>{{ $pesanan->jumlah_tiket }}</td>
                    <td>
                        <a href="{{ route('content.pembeli.tambah', ['id' => $pesanan->id]) }}" class="btn btn-primary">Pemesanan</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
