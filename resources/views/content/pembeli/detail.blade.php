@extends('Layout.loged-layout')

@section('content')

<body body class="main-color">
    <div id="app">
        <div class="main-wrapper">
            <div class="main-content">
                <div class="container">
                    <form action="{{ url('index', $ticket->id)}}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('index') }}
                    <div class="card mt-5">
                        <div class="card-header">
                            <h3>Detail Tikets</h3>
                            
                        </div> 
                        <div class="card-body" style="text-align: center">
                            <img width="600px" src="{{asset($ticket->image)}}" alt="kosong">
                        </div>
                        <div class="card-body">
                            <h3>Deskripsi {{$ticket->title}}</h3><br>
                            <p>{{$ticket->deskripsi}}</p>

                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Stock</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <td>{{$ticket->title}}</td>
                                    <td>{{$ticket->name}}</td>
                                    <td>{{$ticket->price}}</td>
                                    <td>{{$ticket->stock}}</td>
                                    {{-- <tr>
                                        <td data-label="Image"><img src="{{ asset($ticket->image) }}"
                                                class="img-thumbnail" alt="Ticket Image"></td>
                                        <td data-label="ID">{{ $ticket->id }}</td>
                                        <td data-label="Title">{{ $ticket->title }}</td>
                                        <td data-label="Name Band">{{ $ticket->name }}</td>
                                        <td data-label="Price">{{ $ticket->price }}</td>
                                        <td data-label="Stock">{{ $ticket->stock }}</td>
                                        <td data-label="Detail">
                                        <a href="{{'detail/'.$ticket->id}}" class="btn btn-warning"> Detail</a>
                                        </td> --}}
                                        {{-- <td> <a class="btn btn-warning " href="{{ route('content.pembeli.detail') }}">Detail</a>
                                        </td> --}}
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

@endsection