@extends('Layout.loged-layout')

@section('content')
</div>

<body body class="main-color">
    <div id="app">
        <div class="main-wrapper">
            <div class="main-content">
                <div class="container">
                    <div class="card mt-5">
                        <div class="card-header">
                            <h3>List Tickets</h3>
                        </div>
                        <div class="card-body">
                            @if (session('success'))
                            <div class="alert alert-success">{{ session('success') }}</div>
                            @endif

                            @if (session('error'))
                            <div class="alert alert-danger">{{ session('error') }}</div>
                            @endif
                            <p>
                                <a class="btn btn-primary" href="{{ route('content.pembeli.create') }}">New
                                    Ticket</a>
                            </p>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Stock</th>
                                        <th>
                                            action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($tickets as $ticket)
                                    <tr>
                                        <td data-label="Image"><img src="{{ asset($ticket->image) }}"
                                                class="img-thumbnail" alt="Ticket Image"></td>
                                        <td data-label="ID">{{ $ticket->id }}</td>
                                        <td data-label="Title">{{ $ticket->title }}</td>
                                        <td data-label="Name Band">{{ $ticket->name }}</td>
                                        <td data-label="Price">{{ $ticket->price }}</td>
                                        <td data-label="Stock">{{ $ticket->stock }}</td>
                                        <td data-label="Detail">
                                        <a href="{{'detail/'.$ticket->id}}" class="btn btn-warning"> Detail</a>
                                        </td>
                                        {{-- <td> <a class="btn btn-warning " href="{{ route('content.pembeli.detail') }}">Detail</a>
                                        </td> --}}
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="6">
                                            No record found!
                                        </td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@endsection
