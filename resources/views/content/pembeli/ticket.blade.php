<!DOCTYPE html>
<html lang="en">

<head>
   <!-- basic -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- mobile metas -->
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="viewport" content="initial-scale=1, maximum-scale=1">
   <!-- site metas -->
   <title>Ticketo</title>
   <meta name="keywords" content="">
   <meta name="description" content="">
   <meta name="author" content="">
   <!-- bootstrap css -->
   <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
   <!-- style css -->
   <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
   <!-- Responsive-->
   <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
   <!-- fevicon -->
   <link rel="icon" href="{{ asset('images/fevicon.png') }}" type="image/gif" />
   <!-- Scrollbar Custom CSS -->
   <link rel="stylesheet" href="{{ asset('css/jquery.mCustomScrollbar.min.css') }}">
   <!-- Tweaks for older IEs-->
   <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
   <!-- fonts -->
   <link href="https://fonts.googleapis.com/css?family=Poppins:400,700|Righteous&display=swap" rel="stylesheet">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
   <!-- owl stylesheets -->
   <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
   <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
      media="screen">
</head>

<body class="main-color">
   <!-- header section start -->
   <div class="header_section">
      <div class="header_main">
         <div class="container-fluid">
            <div class="logo"><h1 class="header-font">Ticketo</h1></div>
            <div class="menu_main">
               <ul>
                  <li class="active"><a href="home">Home</a></li>
                  <li><a href="about">About</a></li>
                  <li><a href="services">Services</a></li>
                  <li><a href="blog">Blog</a></li>
                  <li><a href="contact">Contact us</a></li>
               </ul>
            </div>
         </div>
      </div>
      </div>
      <!-- banner section start -->
      {{-- <div class="banner_section layout_padding">
         <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="">
               <div class="carousel-item active">
                  <div class="container">
                     <h1 class="banner_taital">SING WITH US</h1>
                     <p class="banner_text">Your favorite famous musician will be with you for a single night</p>
                     <div class="more_bt"><a href="#">Order Tickets</a></div>
                  </div>
               </div>
               <div class="carousel-item">
                  <div class="container">
                     <h1 class="banner_taital">SING WITH US</h1>
                     <p class="banner_text">Your favorite famous musician will be with you for a single night</p>
                     <div class="more_bt"><a href="#">Order Tickets</a></div>
                  </div>
               </div>
               <div class="carousel-item">
                  <div class="container">
                     <h1 class="banner_taital">SING WITH US</h1>
                     <p class="banner_text">Your favorite famous musician will be with you for a single night</p>
                     <div class="more_bt"><a href="#">Order Tickets</a></div>
                  </div>
               </div>
            </div>
         </div>
      </div> --}}
   </div>
   <!-- header section end -->
   <!-- services section start -->
   <div class="services_section layout_padding">
      <div class="container">
		<div class="heading_container text-center">
			<h2 class="custom_heading">Our Concerts</h2>
		 </div>		 
         <div class="row">
            <div class="col-md-4">
               <div class="box">
                  <img src="{{ asset('images/konser_1.jpg') }}" alt="" class="img-fluid">
                  <div class="box_contant">
                     <h3 class="box_head">Justin Bieber</h3>
                     <p class="box_text">Jakarta, Indonesia</p>
                     <a href="detail/justin" class="read_more">Read More</a>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="box">
                  <img src="{{ asset('images/konser_2.jpg') }}" alt="" class="img-fluid">
                  <div class="box_contant">
                     <h3 class="box_head">Coldplay</h3>
                     <p class="box_text">Jakarta, Indonesia</p>
                     <a href="detail/coldplay" class="read_more">Read More</a>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="box">
                  <img src="{{ asset('images/konser_3.jpg') }}" alt="" class="img-fluid">
                  <div class="box_contant">
                     <h3 class="box_head">Maroon 5</h3>
                     <p class="box_text">Jakarta, Indonesia</p>
                     <a href="detail/marron5" class="read_more">Read More</a>
                  </div>
               </div>
            </div>
			<div class="col-md-4">
				<div class="box">
				   <img src="{{ asset('images/konser_4.jpg') }}" alt="" class="img-fluid">
				   <div class="box_contant">
					  <h3 class="box_head">Bruno Mars</h3>
					  <p class="box_text">Jakarta, Indonesia</p>
					  <a href="detail/brunomars" class="read_more">Read More</a>
				   </div>
				</div>
			 </div>
			<div class="col-md-4">
				<div class="box">
				   <img src="{{ asset('images/konser_5.jpg') }}" alt="" class="img-fluid">
				   <div class="box_contant">
					  <h3 class="box_head">Alan Walker</h3>
					  <p class="box_text">Jakarta, Indonesia</p>
					  <a href="detail/alanwalker" class="read_more">Read More</a>
				   </div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="box">
				   <img src="{{ asset('images/konser_6.jpg') }}" alt="" class="img-fluid">
				   <div class="box_contant">
					  <h3 class="box_head">The Weekend</h3>
					  <p class="box_text">Jakarta, Indonesia</p>
					  <a href="detail/theweekend" class="read_more">Read More</a>
				   </div>
				</div>
			 </div>
         </div>
      </div>
   </div>
   <!-- services section end -->
   <!-- copy right section start -->
   <div class="copyright_section color-dark">
	<div class="container">
	   <ul class="font">
		  <li>Facebook</li>
		  <li>Instagram</li>
		  <li>&copy; 2023 All Right Reserved</li>
	   </ul>
	</div>
 </div>
   <!-- copy right section end -->
   <!-- Javascript files-->
   <script src="{{ asset('js/jquery.min.js') }}"></script>
   <script src="{{ asset('js/popper.min.js') }}"></script>
   <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
   <script src="{{ asset('js/jquery-3.0.0.min.js') }}"></script>
   <script src="{{ asset('js/plugin.js') }}"></script>
   <!-- sidebar -->
   <script src="{{ asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
   <script src="{{ asset('js/custom.js') }}"></script>
   <!-- javascript -->
   <script src="{{ asset('js/owl.carousel.js') }}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
</body>

</html>
