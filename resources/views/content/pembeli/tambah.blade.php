@extends('Layout.loged-layout')

@section('content')
    <div class="container">
        <h1>Tambah Pemesanan</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('content.pembeli.pemesanan.store') }}" method="POST">
            @csrf

            <div class="form-group">
                <label for="id_pembeli">ID Pembeli</label>
                <select class="form-control" id="id_pembeli" name="id_pembeli">
                    @foreach($pembeli as $p)
                        <option value="{{ $p->id }}">{{ $p->nama }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="id_tiket">ID Tiket</label>
                <select class="form-control" id="id_tiket" name="id_tiket">
                    @foreach($tickets as $t)
                        <option value="{{ $t->id }}">{{ $t->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="jumlah_tiket">Jumlah Tiket</label>
                <input type="number" class="form-control" id="jumlah_tiket" name="jumlah_tiket">
            </div>

            <button type="submit" class="btn btn-primary">Tambah Pemesanan</button>
        </form>
    </div>
@endsection
