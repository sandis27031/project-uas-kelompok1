<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Marron 5</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style-kami.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body style="background-color: rgba(237, 230, 230, 0.921)">
    <div class="container">
        <main class="mt-2">
            {{-- Ringkasan --}}
            <div class="mt-2" id="highlight">
                <div class="row">
                    <div class="col-6">
                        <h4>Highlight</h4>
                        <p style="text-align: justify">Maroon 5 adalah sebuah grup musik asal Amerika Serikat yang beraliran pop rock. Grup musik ini dibentuk di California, Amerika Serikat.
                           Grup musik ini awalnya terbentuk pada tahun 1997 dengan nama Kara's Flowers saat mereka masih bersekolah di Sekolah Menengah Atas. Dengan anggota Adam Levine, Mickey Madden, Jesse Carmichael dan Ryan Dusick, mereka menandatangani Reprise Record dan meluncurkan album The Fourth World pada tahun 1997. 
                           Setelah tanggapan yang hangat pada album tersebut, band ini memisahkan diri dari label rekaman mereka dan masuk perguruan tinggi. Pada tahun 2001, band ini bersatu kembali dan menambah James Valentine menjadi anggotanya, dan menuju arah yang baru di bawah nama Maroon 5.</p>
                    </div>
                    <div class="col-6 ">
                        <div class="mx-auto">
                          <img src="{{ asset('images/konser_3.jpg') }}" alt="" width="280" class="mb-1">
                          <img src="{{ asset('images/konser_3.jpg') }}" alt="" width="280" class="mb-1">
                          <img src="{{ asset('images/konser_3.jpg') }}" alt="" width="280" class="mb-1">
                          <img src="{{ asset('images/konser_3.jpg') }}" alt="" width="280" class="mb-1">
                        </div>
                    </div>
                </div>

            </div>
            {{-- Daftar Tiket --}}
            <div class="mt-2" id="daftartiket">
              <h4>Daftar Harga Tiket</h4>
              <div class="row">
                <div class="col-6">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Tempat</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Action </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>VIP</td>
                        <td>Rp 4.000.000</td>
                        <td><button class="btn btn-warning">Beli Tiket</button></td>
                      </tr>
                      <tr>
                        <th scope="row">2</th>
                        <td>CAT 1</td>
                        <td>Rp 3.500.000</td>
                        <td><button class="btn btn-warning">Beli Tiket</button></td>
                      </tr>
                      <tr>
                        <th scope="row">3</th>
                        <td>CAT 2</td>
                        <td>Rp 3.000.000</td>
                        <td><button class="btn btn-warning">Beli Tiket</button></td>
                      </tr>
                      <tr>
                        <th scope="row">4</th>
                        <td>CAT 2</td>
                        <td>Rp 2.000.000</td>
                        <td><button class="btn btn-warning">Beli Tiket</button></td>
                      </tr>
                      <tr>
                        <th scope="row">5</th>
                        <td>CAT 3</td>
                        <td>Rp 1.800.000</td>
                        <td><button class="btn btn-warning">Beli Tiket</button></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

            </div>
         
        </main>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"
        integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous">
    </script>
</body>

</html>
