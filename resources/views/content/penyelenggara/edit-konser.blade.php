@extends('Layout.penyelenggara-loged-layout')

@section('content')

    @include('Component.sidebar')
    <section class="home-section">
        <h1 class="header-font" style="padding-left: 10px">Add Concert</h1>
        <div class="form-container">
            <form action="{{ url('edit-konser', $konser->id)}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group mb-3">
                    <label class="form-label">Concert Name</label>
                    <input type="text" class="form-control" id="nama_konser" name="nama_konser"
                        value="{{$konser->nama_konser}}">
                </div>
                <div class="form-group mb-3">
                    <label class="form-label">Date</label>
                    <input type="date" class="form-control" id="date" name="date"
                        value="{{$konser->date}}">
                </div>
                <div class="form-group mb-3">
                    <label class="form-label">Time</label>
                    <input type="time" class="form-control" id="time" name="time"
                        value="{{$konser->time}}">
                </div>
                <div class="form-group mb-3">
                    <label class="form-label">Description</label>
                    <textarea name="deskripsi" id="deskripsi" cols="30" rows="5" class="form-control" 
                    value="{{$konser->deskripsi}}"></textarea>
                </div>
                <div class="form-group mb-3">
                    <label class="form-label">Image</label>
                    <input class="form-control" type="file" id="image" name="image"
                        value="{{$konser->image }}">
                </div>
                <button class="light-button upper-font bold-font"><a type="submit">Save</a></button>
            </form>
        </div>
    </section>

@endsection