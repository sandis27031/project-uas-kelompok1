@extends('Layout.penyelenggara-loged-layout')

@section('content')
    @include('Component.sidebar')
    <section class="home-section">
        <div class="row">
            <div class="col-8">
                <h1 class="header-font" style="padding-left: 10px; padding-top: 10px">Concert Details</h1>
            </div>
            <div class="col-4" style="width: 100%; text-align: right; padding-top: 10px">
                <form action="{{ url('delete-konser', $konser->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('delete') }}
                    <a class="btn" href="{{ 'edit-konser/' . $konser->id }}"
                        style="background: transparent; 
                            border: transparent;
                            padding-bottom: 35px">
                        <i class='bx bxs-edit'
                            style="font-size: 50px; 
                                color: #eef4ed; 
                                padding-right: 10px;"></i>
                    </a>
                    <button type="submit" style="background: transparent">
                        <i class='bx bxs-trash' style="font-size: 50px; color: #eef4ed"></i>
                    </button>
                </form>
            </div>
        </div>
        <div class="detail-container">
            <div class="row">
                <div class="img-detail col" style="text-align: center">
                    <img class="detail-image" src="{{ asset($konser->image) }}" alt="">
                </div>
                <div class="detail-concert col">
                    <h1>{{ $konser->nama_konser }}</h1>
                    <div class="row">
                        <div class="col-4">
                            <i class='bx bx-calendar-star' style="font-size: 20px"> {{ $konser->date }}</i>
                        </div>
                        <div class="col-4">
                            <i class='bx bx-time-five' style="font-size: 20px"> {{ $konser->time }}</i>
                        </div>
                    </div>
                    <p style="padding-top: 10px">{{ $konser->deskripsi }}</p>
                </div>
            </div>
            <div>
                <div class="row">
                    <div class="col-8" style="padding-top: 20px">
                        <h3>Ticket List</h3>
                    </div>
                    <div class="col-4" style="width: 100%; text-align: right">
                        <a href="#">
                            <i class='bx bx-plus-circle' style="font-size: 48px"></i>
                        </a>
                    </div>
                </div>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Ticket ID</th>
                            <th>Stock</th>
                            <th>Price</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection
