@extends('Layout.penyelenggara-loged-layout')

@section('content')

    @include('Component.sidebar')
    <section class="home-section">
        <h1 class="header-font" style="padding-left: 10px">Add Concert</h1>
        <div class="form-container">
            <form action="tambah-konser" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" id="id_penyelenggara" name="id_penyelenggara" value="{{ Auth::user()->id }}">
                <div class="form-group mb-3">
                    <label class="form-label">Concert Name</label>
                    <input type="text" class="form-control" id="nama_konser" name="nama_konser">
                </div>
                <div class="form-group mb-3">
                    <label class="form-label">Date</label>
                    <input type="date" class="form-control" id="date" name="date">
                </div>
                <div class="form-group mb-3">
                    <label class="form-label">Time</label>
                    <input type="time" class="form-control" id="time" name="time">
                </div>
                <div class="form-group mb-3">
                    <label class="form-label">Description</label>
                    <textarea name="deskripsi" id="deskripsi" cols="30" rows="5" class="form-control"></textarea>
                </div>
                <div class="form-group mb-3">
                    <label class="form-label">Image</label>
                    <input class="form-control" type="file" id="image" name="image">
                </div>
                <button class="light-button upper-font bold-font"><a type="submit">Save</a></button>
            </form>
        </div>
    </section>

@endsection