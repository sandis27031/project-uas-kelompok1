@extends('Layout.penyelenggara-loged-layout')

@section('content')

@include('Component.sidebar')
  <section class="home-section">
    <h1 class="dashbrod">Dashboard Data Statistik Penjualan Tiket Konser</h1>
    
    <div class="row">
      <div class="col-md-6">
        <div class="card mb-3">
          <div class="card-body">
            <h5 class="card-title">Total Income</h5>
            <p class="card-text">IDR {{ DB::table('invoice')->sum('total_bayar') }}</p>
            <div class="light-button upper-font bold-font"><a href="#">More Info</a></div>

          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card mb-3">
          <div class="card-body">
            <h5 class="card-title">Sold Ticket</h5>
            <p class="card-text">{{ DB::table('invoice')->count() }} tiket</p>
            <div class="light-button upper-font bold-font"><a href="#">More Info</a></div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Grafik Penjualan Tiket</h5>
        <canvas id="salesChart"></canvas>
      </div>
    </div>
  </section>
  
  <script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.0/dist/chart.min.js"></script>
  <script>
    var salesData = {
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
      datasets: [{
        label: 'Penjualan Tiket',
        data: [50, 75, 60, 80, 65, 90],
        backgroundColor: 'rgba(75, 192, 192, 0.6)',
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 1
      }]
    };
    
    var salesConfig = {
      type: 'bar',
      data: salesData,
      options: {
        scales: {
          y: {
            beginAtZero: true,
            precision: 0
          }
        }
      }
    };
    
    var salesChart = document.getElementById('salesChart').getContext('2d');
    new Chart(salesChart, salesConfig);
    
    /*$.ajax({
        url: '/fetch-chart-data',
        method: 'GET',
        dataType: 'json',
        success: function(data) {
            renderChart(data);
        },
        error: function(error) {
            console.error('Error:', error);
        }
    });


    function renderChart(data) {
        var ctx = document.getElementById('dashChart').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: data.map(item => item.tanggal_bayar),
                datasets: [{
                    label: 'Income',
                    data: data.map(item => item.total_bayar),
                    backgroundColor: 'rgba(0, 123, 255, 0.5)',
                    borderColor: 'rgba(0, 123, 255, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                // Customize chart options as needed
            }
        });
    }
*/
</script>
@endsection