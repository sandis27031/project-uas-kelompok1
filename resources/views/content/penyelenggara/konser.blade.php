@extends('Layout.penyelenggara-loged-layout')

@section('content')

@include('Component.sidebar')

<section class="home-section">
    <div class="container">
        <div class="row">
            <div class="col-8">
                <h1 class="header-font" style="padding-top: 40px;">List Konser</h1>
            </div>
            <div class="col-4">
                <button class="light-button upper-font bold-font" style="background: transparent; margin-top: 0px"><a href="#">Add</a></button>
            </div>
        </div>    
        <div class="row">
            @foreach ($konser as $ksr)

            <div class="card card-concert">
                <img class="card-img-top" src="{{ asset($ksr->image) }}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title" style="color: black">{{ $ksr->nama_konser}}</h5>
                    <p class="card-text" style="color: black">{{ $ksr->date }} {{$ksr->time}}</p>
                </div>
                <a href="{{'konser-detail/'. $ksr->id}}" style="background: red">
                <div class="card-footer">
                    <div class="row">
                        <div class="col-8">
                            <button class="upper-font bold-font" style="margin-top: 0; padding: 0">
                                <a href="{{'konser-detail/'.$ksr->id}}">Details</a>
                            </button>
                        </div>
                        <div class="col-4">
                            <i class='bx bx-chevron-right' style="width: 100%; text-align: right"></i>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            
            @endforeach
            <div class="card card-concert">
                <div class="card-header" src="..." alt="Card image cap"></div>
                <div class="card-body add" style="text-align: center; width: 100%; height: 100%; padding-top: 40%">
                    <a href="tambah-konser">
                        <i class='bx bx-plus-circle' style="font-size: 78px;"></i>
                    </a>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
    </div>
</section>
@endsection