@extends('Layout.penyelenggara-loged-layout')

@section('content')
    @include('Component.sidebar')

    <section class="home-section">

        <div class="row justify-content-end">
            <div class="col-8">
                <h1 class="header-font" style="padding-left: 10px">Profile</h1>
            </div>
            <div class="col-4 row justify-content-end">
                <div class="red-button right-align">
                    <a class="" href="logout">Logout</a>
                </div>
            </div>
        </div>
        <div class="profile-container">
            <form class="profile-form">
                <div class="row">
                    <div class="col-8 mb-2">
                        <h1 class="header-font">{{ Auth::user()->username }}</h1>
                    </div>
                    <div class="col-4" style="width: 100%; text-align: right">
                        <i class='bx bxs-edit'></i>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name" aria-describedby="nameHelp"
                        value="{{ Auth::user()->nama }}" readonly>
                </div>
                <div class="row">
                    <div class="mb-3 col-8">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email"
                            value="{{ Auth::user()->email }}" readonly>
                    </div>
                    <div class="mb-3 col-4">
                        <label for="no_telp" class="form-label">Phone Number</label>
                        <input type="text" class="form-control" id="no_telp" name="no_telp"
                            value="{{ Auth::user()->no_telp }}" readonly>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="address">Address</label>
                    <textarea name="address" id="address" cols="30" rows="10" class="form-control" readonly>{{ Auth::user()->alamat }}</textarea>
                </div>
            </form>
            <form class="edit-form hide" action="{{ url('update-user', Auth::user()->id) }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="mb-2">
                    <h1 class="header-font">{{ Auth::user()->username }}</h1>
                </div>
                <div class="mb-3">
                    <label for="nama" class="form-label">Name</label>
                    <input type="text" class="form-control" id="nama" name="nama" aria-describedby="nameHelp"
                        value="{{ Auth::user()->nama }}">
                </div>
                <div class="row">
                    <div class="mb-3 col-8">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email"
                            value="{{ Auth::user()->email }}">
                    </div>
                    <div class="mb-3 col-4">
                        <label for="no_telp" class="form-label">Phone Number</label>
                        <input type="text" class="form-control" id="no_telp" name="no_telp"
                            value="{{ Auth::user()->no_telp }}">
                    </div>
                </div>
                <div class="mb-3">
                    <label for="alamat">Address</label>
                    <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <button class="light-button upper-font bold-font"><a href="#">Cancel</a></button>
                <button class="light-button upper-font bold-font"><a type="submit">Save</a></button>
            </form>
        </div>
    </section>

    <script>
        let read = document.querySelector(".profile-form");
        let edit = document.querySelector(".edit-form.hide");
        let editBtn = document.querySelector(".bxs-edit");
        console.log(editBtn);
        editBtn.addEventListener("click", () => {
            read.classList.toggle("read");
            edit.classList.toggle("hide");
        });

        let cancel = document.querySelector(".light-button.upper-font.bold-font");
        cancel.addEventListener("click", () => {
            read.classList.toggle("read");
            edit.classList.toggle("hide");
        });
    </script>
@endsection
