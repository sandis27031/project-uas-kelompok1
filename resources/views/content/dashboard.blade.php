@extends('Layout.loged-layout')

@section('content')
    </div>
    <div class="services_section trending_padding">
        <div class="container">
            <div class="services_section_2">
                <div class="row">
                    <div class="col-md-4">
                        <div><img src="images/blackpink.png" class="services_img"></div>
                        <div class="light-button upper-font bold-font"><a href="#">BlackPink</a></div>
                    </div>
                    <div class="col-md-4">
                        <div><img src="images/justin.png" class="services_img"></div>
                        <div class="light-button upper-font bold-font"><a href="#">Justin Bieber</a></div>
                    </div>
                    <div class="col-md-4">
                        <div><img src="images/coldplay.png" class="services_img"></div>
                        <div class="light-button upper-font bold-font"><a href="#">Coldplay</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="services_section trending_padding">
        <div>
            <table class="table table-bordered">
                <thead>
                    <h3>Ini Tabel Kosong</h3>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            Isinya kosong
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Ini juga kosong
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sama masih kosong
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
