<div class="sidebar">
    <div class="logo-details">
        <i class='bx bxl-deezer'></i>
        <span class="logo-name">MelodicPass</span>
    </div>
    <ul class="nav-links">
        <li>
            <a href="/dashboard-penyelenggara">
                <i class="bx bx-grid-alt"></i>
                <span class="link-name">Dashboard</span>
            </a>
        </li>
        <li>
            <div class="icon-link">
                <a href="/list-konser">
                    <i class="bx bx-collection"></i>
                    <span class="link-name">Concert</span>
                </a>
                <i class="bx bxs-chevron-down arrow"></i>
            </div>
            <ul class="sub-menu">
                <li><a href="#">Ticket</a></li>
                <li><a href="#">A</a></li>
                <li><a href="#">B</a></li>
            </ul>
        </li>
        <li style="
            position: fixed;
            bottom: 0;
            width: 260px;
            display: flex;
            align-items: center;">
            <a href="/profile-penyelenggara">
                <i class='bx bx-user-circle'></i>
                <span class="link-name">User Control</span>
            </a>
        </li>
    </ul>
</div>

<script>

    let arrow = document.querySelectorAll(".arrow");
    console.log(arrow);
    for (var i = 0; i < arrow.length; i++){
        arrow[i].addEventListener("click", (e)=>{
            let arrowParent = e.target.parentElement.parentElement; 
            console.log(arrowParent);
            arrowParent.classList.toggle("showMenu");
        })
    }

    // let sidebar = document.querySelector(".sidebar");
    // let sidebarBtn = document.querySelector(".bxl-deezer");
    // console.log(sidebarBtn);
    // sidebarBtn.addEventListener("click", ()=>{
    //     sidebar.classList.toggle("close");
    // });
</script>