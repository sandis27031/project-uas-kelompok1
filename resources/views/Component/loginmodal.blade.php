<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content main-color">
            <div class="modal-header">
                <h3 class="dark-font modals-title" id="exampleModalLabel">MelodicPass</h3>
                {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> --}}
            </div>
            <div class="modal-body">
                <form id="form-login" action="post-login" method="POST">
                    {{ csrf_field() }}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }} <br />
                            @endforeach
                        </div>
                    @endif
                    <div>
                        <a class="upper-font">Login</a>
                        <input type="email" id="email" placeholder="Email" class="form-control mb-4 mt-2"
                            name="email" required autofocus>
                    </div>
                    <div>
                        {{-- <label for="password">Password</label> --}}
                        <input type="password" id="password" placeholder="Password" class="form-control mb-4 mt-2"
                            name="password" required>
                    </div>
                    <div class="modals-button-layout">
                        <button type="submit" name="register" class="btn-login">Login</button>
                    </div>
                    <p class="font-center">Don't have an account?</p>
                    <button onclick="sembunyiLogin()" class="btn-regist">Register</button>
                </form>

                <form id="form-regist" action="post-register" method="POST">
                    {{ csrf_field() }}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }} <br />
                            @endforeach
                        </div>
                    @endif
                    <div>
                        <a class="upper-font">Register</a>
                        <input type="text" id="nama" placeholder="Nama" class="form-control mb-4 mt-2"
                            name="nama" required autofocus>
                    </div>
                    <div>
                        <input type="text" id="username" placeholder="Username" class="form-control mb-4 mt-2"
                            name="username" required autofocus>
                    </div>
                    <div>
                        <input type="email" id="email" placeholder="Email" class="form-control mb-4 mt-2"
                            name="email" required autofocus>
                    </div>
                    <div>
                        {{-- <label for="password">Password</label> --}}
                        <input placeholder="Password" type="password" id="password" class="form-control mb-4 mt-2"
                            name="password" required>
                    </div>
                    <div>
                        {{-- <label for="password">Password Confirmation</label> --}}
                        <input placeholder="Password Confirmation" type="password" id="password_confirmation"
                            class="form-control mb-4 mt-2" name="password_confirmation" required autofocus>
                    </div>
                    <div class="form-check mt-2">
                        <input class="form-check-input" type="radio" name="role" id="role" value="Pembeli"
                            checked>
                        <label class="form-check-label" for="role1">
                            Pembeli
                        </label>
                    </div>
                    <div class="form-check mb-4">
                        <input class="form-check-input" type="radio" name="role" id="role"
                            value="Penyelenggara">
                        <label class="form-check-label" for="role2">
                            Penyelenggara
                        </label>
                    </div>
                    <div class="modals-button-layout">
                        <button type="submit" name="register" class="btn-login">Register</button>
                    </div>
                    <p class="font-center">Already have an account?</p>
                    <button onclick="sembunyiRegist()" class="btn-regist">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>
