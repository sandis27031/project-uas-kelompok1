<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\KonserController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PemesananController;
use App\Http\Controllers\PembeliController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('Content.landingpage');
})->name('home')->middleware('guest');

Route::post('/post-login', [AuthController::class, 'postLogin']);
Route::post('/post-register', [RegisterController::class, 'store']);
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', function () {
        return view('Content.dashboard');
    });
});

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard-penyelenggara', function () {
        return view('Content.penyelenggara.dashboard-penyelenggara');
    });
});

Route::get('/tampilkan-jumlah', 'InvoiceController@tampilkanJumlah');

Route::get('/fetch-chart-data', 'InvoiceController@fetchChartData');

// Route::group(['middleware' => ['auth', 'checkrole:penyelenggara,pembeli']], function () {
//     Route::get('/dashboard', function(){
//         return
//     });
// });

// Route::group(['middleware' => ['auth', 'checkrole:penyelenggara']], function () {
//     Route::get('/dashboard');
// });

Route::get('/about', function () {
    return view('about');
});
Route::get('/detail', function () {
    return view('content.pembeli.detail');
});
Route::get('/blog', function () {
    return view('blog');
});
Route::get('/services', function () {
    return view('services');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/ticket', function () {
    return view('content.pembeli.ticket');
});
Route::get('tickets', [TicketController::class, 'index'])->name('content.pembeli.index');
Route::get('create-tickets', [TicketController::class, 'create'])->name('content.pembeli.create');
Route::post('tickets', [TicketController::class, 'store'])->name('content.pembeli.store');
Route::get('/dashboard-penyelenggara', function () {
    return view('Content.penyelenggara.dashboard-penyelenggara');
});

//pembeli
Route::get('detail/{id}', [TicketController::class, 'detailtiket']);

Route::get('/detail/justin', function () {
    return view('content.Detail.justin');
});

Route::get('/detail/coldplay', function () {
    return view('content.Detail.coldplay');
});

Route::get('/detail/marron5', function () {
    return view('content.Detail.marron5');
});

Route::get('/detail/brunomars', function () {
    return view('content.Detail.brunomars');
});

Route::get('/detail/alanwalker', function () {
    return view('content.Detail.alanwalker');
});

Route::get('/detail/theweekend', function () {
    return view('content.Detail.theweekend');
});
Route::get('/list-konser', function () {
    return view('Content.penyelenggara.konser');
});

// Penyelenggara
Route::get('/profile-penyelenggara', [UserController::class, 'index']);
Route::put('/update-user/{id}', [UserController::class, 'update']);

// Konser
Route::get('/list-konser', [KonserController::class, 'index'])->name('content.penyelenggara.index');;
Route::get('/tambah-konser', [KonserController::class, 'create'])->name('content.penyelenggara.create');;
Route::post('/tambah-konser', [KonserController::class, 'store'])->name('content.penyelenggara.store');
Route::get('/konser-detail/{id}', [KonserController::class, 'detail']);
Route::get('/konser-detail/edit-konser/{id}', [KonserController::class, 'edit']);
Route::put('/edit-konser/{id}', [KonserController::class, 'update']);
Route::delete('/delete-konser/{id}', [KonserController::class, 'delete']);


