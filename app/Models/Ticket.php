<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'name',
        'price',
        'stock',
        'image',
        'deskripsi',
    ];
    public function pemesanan()
    {
        return $this->hasMany(Pemesanan::class, 'id_tiket');
    }
}
