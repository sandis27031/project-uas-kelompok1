<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Konser extends Model
{
    use HasFactory;
    
    protected $table = 'konsers';
    protected $fillable = [
        'id_konser',
        'id_penyelenggara',
        'nama_konser',
        'date',
        'time',
        'image',
        'deskripsi',
    ];
}
