<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        $data['users'] = User::find(Auth::user()->id);
        return view('Content.penyelenggara.profile-penyelenggara', $data);
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        User::find($id)->update($input);
        return redirect('profile-penyelenggara');
    }
}
