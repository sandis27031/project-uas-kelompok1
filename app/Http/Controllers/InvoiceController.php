<?php

namespace App\Http\Controllers;
use App\Models\Invoice;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    //
    public function tampilkanJumlah()
    {
        $jumlah = DB::table('invoice')->sum('total_bayar');

        return view('totalBayar', compact('jumlah'));
    }

    public function fetchChartData()
    {
        $data = Invoice::select('tanggal_bayar', 'total_bayar')->get();

        return response()->json($data);
    }
}
