<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreKonserRequest;
use App\Models\Konser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class KonserController extends Controller
{
    public function index()
    {
        $data['konser'] = Konser::all();
        return view('content.penyelenggara.konser', $data);
    }

    public function detail($id)
    {
        $data['konser'] = Konser::find($id);
        return view('content.penyelenggara.konser-detail', $data);
    }

    public function create()
    {
        return view('content.penyelenggara.tambah-konser');
    }

    public function store(StoreKonserRequest $request)
    {
        $imageName = time() . '.' . $request->image->extension();
        $uploadedImage = $request->image->move(public_path('images'), $imageName);
        $imagePath = 'images/' . $imageName;
        $params = $request->validated();

        if ($konser = Konser::create($params)) {
            $konser->image = $imagePath;
            $konser->save();

            return redirect('list-konser');
        }
        $request->image->move(public_path('images'), $imageName);
        // public/images/image_name.png
        $request->image->storeAs('images', $imageName);
        // storage/app/images/image_name.png
        $request->image->storeAs('images', $imageName, 's3');
    }

    public function edit($id)
    {
        $data['konser'] = Konser::find($id);
        return view('content.penyelenggara.edit-konser', $data);
    }

    public function update($id, Request $request)
    {
        $konser = Konser::find($id);
        $konser->nama_konser = $request->input('nama_konser');
        $konser->date = $request->input('date');
        $konser->time = $request->input('time');
        $konser->deskripsi = $request->input('deskripsi');
        if ($request->hasFile('image')) {
            $destination = 'images/' . $konser->images;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/', $filename);
            $konser->image = $filename;
            // $request->image->move(public_path('images'), $filename);
            // // public/images/image_name.png
            // $request->image->storeAs('images', $filename);
            // // storage/app/images/image_name.png
            // $request->image->storeAs('images', $filename, 's3');
        }
        $konser->update();
        return redirect('konser-detail/' . $id);
    }
    public function delete($id, Request $request)
    {
        $konser = Konser::find($id);
        $konser->delete($request->all());
        return redirect('list-konser');
    }
}
