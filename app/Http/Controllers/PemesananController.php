<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pemesanan;
use App\Models\Pembeli;
use App\Models\Ticket;

class PemesananController extends Controller
{
    public function index()
    {
        $pemesanan = Pemesanan::all();
        return view('content.pembeli.pemesanan', compact('pemesanan'));
    }


    public function store(Request $request)
    {
        // Validasi input data jika diperlukan
        $request->validate([
            'id_pembeli' => 'required',
            'id_tiket' => 'required',
            'jumlah_tiket' => 'required',
        ]);

        // Buat data pemesanan baru
        $pemesanan = Pemesanan::create([
            'id_pembeli' => $request->id_pembeli,
            'id_tiket' => $request->id_tiket,
            'jumlah_tiket' => $request->jumlah_tiket,
        ]);

        // Jika data pemesanan berhasil ditambahkan, lakukan tindakan sesuai kebutuhan (misalnya: tampilkan pesan sukses, redirect, dll)
        if ($pemesanan) {
            // Lakukan tindakan yang diinginkan, misalnya:
            return redirect()->back()->with('success', 'Data pemesanan berhasil ditambahkan.');
        } else {
            // Jika terjadi kesalahan, tampilkan pesan error atau lakukan tindakan yang sesuai
            return redirect()->back()->with('error', 'Terjadi kesalahan saat menambahkan data pemesanan.');
        }
    }

    public function create()
    {
        // Mendapatkan daftar pembeli dan tiket
        $pembeli = Pembeli::all();
        $tickets = Ticket::all();

        // Menampilkan view tambah pemesanan dengan data pembeli dan tiket
        return view('content.pembeli.tambah', compact('pembeli', 'tickets'));
    }


    public function show($id)
    {
        $pemesanan = Pemesanan::find($id);
        // Lakukan tindakan sesuai kebutuhan, misalnya tampilkan detail pemesanan
        return view('content.pembeli.detail_pemesanan', compact('pemesanan'));
    }

    public function edit($id)
    {
        $pemesanan = Pemesanan::find($id);
        // Lakukan tindakan sesuai kebutuhan, misalnya tampilkan form edit pemesanan
        return view('content.pembeli.edit_pemesanan', compact('pemesanan'));
    }

    public function update(Request $request, $id)
    {
        // Validasi input data jika diperlukan
        $request->validate([
            'id_pembeli' => 'required',
            'id_tiket' => 'required',
            'jumlah_tiket' => 'required',
        ]);

        // Cari data pemesanan berdasarkan ID
        $pemesanan = Pemesanan::find($id);

        if ($pemesanan) {
            // Update data pemesanan
            $pemesanan->id_pembeli = $request->id_pembeli;
            $pemesanan->id_tiket = $request->id_tiket;
            $pemesanan->jumlah_tiket = $request->jumlah_tiket;
            $pemesanan->save();

            // Lakukan tindakan yang diinginkan, misalnya:
            return redirect()->back()->with('success', 'Data pemesanan berhasil diperbarui.');
        } else {
            // Jika data pemesanan tidak ditemukan, tampilkan pesan error atau lakukan tindakan yang sesuai
            return redirect()->back()->with('error', 'Data pemesanan tidak ditemukan.');
        }
    }

    public function destroy($id)
    {
        // Cari data pemesanan berdasarkan ID
        $pemesanan = Pemesanan::find($id);

        if ($pemesanan) {
            // Hapus data pemesanan
            $pemesanan->delete();

            // Lakukan tindakan yang diinginkan, misalnya:
            return redirect()->back()->with('success', 'Data pemesanan berhasil dihapus.');
        } else {
            // Jika data pemesanan tidak ditemukan, tampilkan pesan error atau lakukan tindakan yang sesuai
            return redirect()->back()->with('error', 'Data pemesanan tidak ditemukan.');
        }
    }
}
