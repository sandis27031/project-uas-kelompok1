<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function postLogin(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt($credentials)) {
            $request->session('')->regenerate();

            if (Auth::user()->role == 'Penyelenggara') {
                return redirect()->intended('dashboard-penyelenggara');
            } elseif (Auth::user()->role == 'Pembeli') {
                return redirect()->intended('dashboard');
            }
        }

        return back()->withErrors([
            'username' => 'Akun belum terdaftar'
        ])->onlyInput('username');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('home');
    }
}
