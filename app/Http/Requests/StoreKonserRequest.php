<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreKonserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id_konser' => [''],
            'id_penyelenggara' => ['required', 'max:20'],
            'nama_konser'=> ['required', 'max:20'] ,
            'date' => [''],
            'time' => [''],
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'deskripsi' => ['required', 'max:255']
        ];
    }
}
